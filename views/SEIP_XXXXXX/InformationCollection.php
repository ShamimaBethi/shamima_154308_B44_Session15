<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Student Information and Marks Collection Form</title>

    <style>

          .headline{
              text-align: center;
              border: solid 1px;

          }

          .studentInfo{
              margin-left: 300px;
              margin-right: 300px;
              border: solid 1px;
              margin-top: 50px;

          }

          .marksInfo{
              margin-left: 300px;
              margin-right: 300px;
              border: solid 1px;
              margin-top: 50px;
          }

          .submitForm{
              text-align: center;
              margin-left: 300px;
              margin-right: 300px;
              border: solid 1px;
              margin-top: 50px;

          }

          .container{
              width: 75%;
              height: 600px;
              margin: 0 auto;
              border: 4px solid black;

          }

    </style>


</head>
<body>

<div class="container">



    <div class="headline">

        <h1> Student Information and Marks Collection Form</h1>

    </div>

    <form action="process.php" method="post">

        <div class="studentInfo">

              Name:<br>
              <input type="text" name="name" > <br>
              Student ID:<br>
              <input type="text" name="studentID" >  <br>

        </div>


        <div class="marksInfo">

            Mark Obtained in Bangla:<br>
            <input type="number" step="any" name="markBangla" > <br>
            Mark Obtained in English:<br>
            <input type="number" step="any" name="markEnglish" >  <br>
            Mark Obtained in Math:<br>
            <input type="number" step="any" name="markMath" > <br>


        </div>


        <div class="submitForm">

            <input type="submit">

        </div>

    </form>

</div>

</body>
</html>